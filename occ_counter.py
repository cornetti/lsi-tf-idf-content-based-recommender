import filereader as fr
import io
from collections import defaultdict
from gensim import utils
#import pprint as pp


tlist = fr.makefilelist()
filec = fr.cleanfiles(fr.readfiles(tlist))

#funzione di supporto per avere ottenere la seconda parte di una tupla
def get_key(item):
    return item[1]

#ritorna il numero di occorrenze di ogni parola in ordine decrescente di utilizzo
def find_occurences(stoplist):
    #keys = filec.keys()
    if (stoplist == ""):
        texts = [[word for word in doc.lower().split()]
                 for doc in filec]
    else:
        texts = [[word for word in doc.lower().split() if word not in stoplist]
                 for doc in filec]

    # remove words that appear only once
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1

    texts = [[token for token in text if frequency[token] > 1] for text in texts]

    tokens = [(token,frequency[token]) for token in frequency]

    return sorted(tokens,key=get_key)[::-1]


#stampa una tabella leggibile da gnuplot e su terminale
def gnuprint(table,filename):

	#scrive in file
    fw = open(filename,"w")
    fw.write("# " + filename + "\n")
    fw.write( "# X Y" + "\n")
    i = 1
    
    #stampa su terminale
    for row in table:
        fw.write("  " + str(i) + " " + str(row[1]) + "\n")
        print str(i) + ". " + row[0] + " : " + str(row[1])
        i += 1

#legge i file nella cartella bbc e stampa le 500 parole piu usate
#in formato tabellare per gnuplot e su terminale
#(prima senza stopwords e poi con le stopwords)

#for c in filec:
#   print "------------------------------------------ " + c + " ----------------------------------------------------"
#   print filec[c]
stopwords = fr.filereadtostr("stopwords.txt")
stoplist = set(stopwords.split())

print "-------------------   WITHOUT STOPWORDS  ------------------------"
occur = find_occurences("")[0:500]
gnuprint(occur,"nostops.dat")
#pp.pprint(occur)

print "-------------------  WITH STOPWORDS -----------------------"
occur = find_occurences(stoplist)[0:500]
gnuprint(occur,"wstops.dat")
#pp.pprint(occur)
