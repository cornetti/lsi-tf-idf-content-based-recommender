import os
import re
import io

maindir = "bbc"


#funzione che genera una lista di file da aprire controllando la cartella bbc
def makefilelist():
    filelist = []
    try:
        for folder in os.listdir(maindir):
            try:
                for filename in os.listdir(maindir + "/" + folder)[0:250]:
                    filedir = maindir + "/" + folder + "/" + filename
                    filelist.append(filedir)
            except (OSError):
                print "errore nell' aprire " + filename
    except (OSError):
        print "errore apertura directory"
    print "numero file da aprire:" + str(len(filelist))
    return filelist


#ritorna la lista di documenti da utilizzare per le analisi
def readfiles(filenames):
    fileconts = []
    titles = []
    for filename in filenames:
        with open(filename) as f:
            filec = f.read()
            f.close()

            #prendo il titolo della notizia
            title = filec.split("\n")[0]

            #elimino i doppioni utilizzando solo i titoli
            if title not in titles:
                fileconts.append(filec)
                titles.append(title)
    print "numero di documenti unici letti: " + str(len(fileconts))
    return fileconts


#pulisce i documenti letti eliminando la punteggiatura
def cleanfiles(fileconts):
    cleaned = []
    for doc in fileconts:
        doc = doc.lower()
        doc = re.sub("[!#$%&'\"()*+,-./:;<=>?@\[\\\\\]^_`{|}~]", ' ', doc)
        cleaned.append(doc)
    return cleaned

#legge il file nella directory passata come argomento
def filereadtostr(filename):
    try:
        file = open(filename,"r")
        string = file.read()
        file.close()
        return string
    except:
        print "file error"
        return ""
