import filereader as fr #legge e pulisce tutte le notizie
from gensim import corpora,similarities,models
import pprint as pp

#leggo dal filesystem i documenti ed elimino la punteggiatura
tlist = fr.makefilelist()
filec = fr.cleanfiles(fr.readfiles(tlist))

#debug: stampa tutti i file
#for c in filec:
#    print "------------------------------------------ " + c + " ----------------------------------------------------"
#    print filec[c]


#lettura file stopwords
stopwords = fr.filereadtostr("stopwords.txt")
stoplist = set(stopwords.split())

#preparo i documenti ed elimino le stopwords
texts = [[word for word in doc.lower().split() if word not in stoplist]
         for doc in filec]

#conto le occorrenze delle parole
from collections import defaultdict
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1

#elimino le parole con occorrenza < 1
texts = [[token for token in text if frequency[token] > 1] for text in texts]

#debug: stampa tutte le parole con occorrenza > 1
#from pprint import pprint
#pprint(texts)

#creo dizionario principale
dictionary = corpora.Dictionary(texts)

#debug: salva dizionario e lo stampa
#dictionary.save('bbc.dict')
#print(dictionary)

#creo il corpus
corpus = [dictionary.doc2bow(text) for text in texts]
#corpora.MmCorpus.serialize('bbc.mm', corpus)

#applico la trasformazione tf-idf al corpus
tfidf = models.TfidfModel(corpus) # step 1 -- initialize a model
corpus_tfidf = tfidf[corpus] # step 2 -- apply transformation

#calcolo la matrice di similarita'
index = similarities.MatrixSimilarity(corpus_tfidf, num_features = len(dictionary))


#lista di articoli interessanti
l = [123,78,115,237,34,239,71,134,225,764,134,9,10,181,53,7,168,91,268,234,19]

#calcolo la media degli scores
scores = index[corpus_tfidf[l[0]]]/(len(l))

i = 0
for k in l[1:]:
    scores += (index[corpus_tfidf[k]])/(len(l))
    print "selected article " + str(i) + ": " + filec[k].split("\n")[0]
    i += 1

print ""

top = sorted(enumerate(scores), key=lambda (k, v): v, reverse=True)

i = 1
for element in top[len(l):50+len(l)]:
    x = element[0]
    print str(i) + " [" + str(x) + "]. " + filec[x].split("\n")[0] + " : " + str(element[1]*100) + "% simile alle notizie date"
    i += 1
